package com.example.lab2.DTO;

public record ProductDTO(String label, String description, float price, int quantity) {
}
