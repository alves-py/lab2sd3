# Readme - Projeto de Gerenciamento de Produtos

Este é um projeto de API RESTful desenvolvido para gerenciar produtos em um banco de dados. A API oferece operações básicas de criação, leitura, atualização e exclusão (CRUD) de produtos, bem como métodos para pesquisar produtos por rótulo (label) e preço.

## Tecnologias Utilizadas

- **Spring Boot**: Framework utilizado para o desenvolvimento da aplicação.
- **Java**: Linguagem de programação principal do projeto.
- **Spring Data JPA**: Biblioteca para acesso a dados com Java Persistence API.
- **ElephantSQL Database**: Banco de dados online postgresql.
- **Maven**: Gerenciador de dependências e construção do projeto.

## Funcionalidades da API

- **Criação de Produto**: Permite adicionar um novo produto ao banco de dados.
- **Listagem de Produtos**: Retorna uma lista de todos os produtos armazenados no banco de dados.
- **Exclusão de Produto**: Remove um produto do banco de dados com base no seu ID.
- **Atualização de Produto**: Atualiza os dados de um produto existente no banco de dados.
- **Pesquisa por Rótulo**: Permite buscar um produto pelo seu rótulo (label).
- **Pesquisa por Preço**: Permite buscar um produto pelo seu preço.

## Endpoints da API

- **POST /**: Cria um novo produto no banco de dados.
- **GET /**: Retorna todos os produtos armazenados.
- **DELETE /{id}**: Remove um produto do banco de dados com base no seu ID.
- **PUT /{id}**: Atualiza os dados de um produto existente no banco de dados.
- **GET /name/{label}**: Busca um produto pelo seu rótulo (label).
- **GET /price/{price}**: Busca um produto pelo seu preço.

## Uso da Classe ControllerAPI

A classe `ControllerAPI` é responsável por definir os endpoints da API e os métodos que executam as operações sobre os produtos no banco de dados. Abaixo estão detalhadas as principais funcionalidades implementadas:

- `createProduct`: Cria um novo produto no banco de dados com base nos dados fornecidos no corpo da requisição.
- `getProducts`: Retorna todos os produtos armazenados no banco de dados.
- `deleteProduct`: Remove um produto do banco de dados com base no ID fornecido na URL.
- `changeProduct`: Atualiza os dados de um produto existente no banco de dados com base no ID fornecido na URL e nos dados fornecidos no corpo da requisição.
- `searchByLabel`: Busca um produto pelo seu rótulo (label).
- `searchByPrice`: Busca um produto pelo seu preço.

Certifique-se de seguir corretamente a estrutura dos endpoints e os tipos de dados esperados para uma integração adequada com a API.

## Execução do Projeto

Para executar o projeto localmente, siga estas etapas:

1. Certifique-se de ter o Java JDK e o Maven instalados em seu sistema.
2. Clone este repositório em sua máquina local.
3. Navegue até o diretório raiz do projeto.
4. Execute o comando `mvn spring-boot:run` no terminal para iniciar o servidor.
5. Acesse os endpoints da API usando um cliente REST como Postman ou cURL.

